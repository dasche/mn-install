#!/bin/bash
################################################################################
# Original Author: TheWeakShall
# Program: SetMeUpNow.sh
# Purpose: Simple install script for MTS MasterNode
#
#	This will only install the basic's binaries & services
#
#	Run this script by using the command
#
#           bash SetMNupFULL.sh
#
################################################################################
output() {
    printf "\E[0;33;40m"
    echo $1
    printf "\E[0m"
}

displayErr() {
    echo
    echo $1;
    echo
    exit 1;
}

    output " "
    output "Welcome to MTS MasterNode Install Script"
    output "This script will ask you for: "
    output "MasterNode key (Generatted In MTS Wallet) "
    output "External IP (Make sure Port 7555 is open)"
    output "Username for RPC To Use"
    output "Password For RPC User"
    output " Please make sure you have the coins in the correct place "
    output " Also recommend Having your wallet open with debug console up "
    output " "
    output " This script will do the following"
    output " Create MethuselahD service  (start|stop|restart|chain)"
    output " install binaries into /usr/local/methuselah "
    output " "
    output " Do Not Run This Script As Root!!!! "
    output " just make sure you have correct sudo access"
    read -e -p "MasterNode Key (Use debug console and typein 'masternode genkey'): " MKey
    read -e -p "External IP  : " EXIP
    read -e -p "RPC Username (can be anything) : " USER
    read -e -p "RPC password (can be anything): " USERPASS

    output " "
    output " and away we go =) ...."
    output " "
    sleep 1
   
    output " "
    output "installing libboost 1.58"
    output " "
    output " there maybe a window that pops up click yes or presss enter"
    sleep 1
    output "and away we go =) ...."
    output " "
    sleep 1
    sudo apt-get update
    sudo apt-get install software-properties-common python-software-properties -y
    sudo add-apt-repository ppa:bitcoin/bitcoin -y
    sudo apt-get update
    sudo apt-get install libdb4.8-dev libdb4.8++-dev libzmq3-dev libboost-all-dev libevent-dev libminiupnpc-dev -y
        
    output " "
    output "Installing fail2ban"
    output " "
    sleep 2

    sudo apt-get -y install fail2ban
    sudo service fail2ban restart
    sudo apt-get install ufw -y

    output " "
    output "Configuring fail2ban"
    output " "
    sleep 3

    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw allow ssh
    sudo ufw allow 7555/tcp
    sudo ufw allow 22/tcp
    sudo ufw limit 22/tcp
    echo -e "${YELLOW}"
    sudo ufw --force enable
    echo -e "${NC}"

    output " "
    output "Preparing enviornment"
    output " "
    sleep 2

    sudo mkdir -p /usr/local/methuselah/bin
    sudo mkdir -p /usr/local/methuselah/src
    sudo mkdir -p /usr/local/methuselah/.methuselah
    sudo sed -i -e 's/usr\/local\/games/usr\/local\/methuselah\/bin/g' /etc/environment
    sudo touch /usr/local/methuselah/.methuselah/methuselah.conf

    output " "
    output "Installing Methuselah"
    output " "
    sleep 3

    sudo wget https://github.com/methuselah-coin/methuselah/releases/download/v1.1.0.2/methuselah-1.1.0.1-linux.tar.xz
    sudo tar -xf methuselah-1.1.0.1-linux.tar.xz -C /usr/local/methuselah/bin
    sleep 3

    sudo chown -R root:root /usr/local/methuselah
    echo "rpcallowip=127.0.0.1
rpcuser=$USER
rpcpassword=$USERPASS
server=1
daemon=1
listen=1
maxconnections=256
masternode=1
masternodeprivkey=$MKey
externalip=$EXIP
promode=1" |sudo tee -a /usr/local/methuselah/.methuselah/methuselah.conf >/dev/null

        sleep 3

        sudo cp service /etc/init.d/methuselah
        sudo chmod 755 /etc/init.d/methuselah
        sudo update-rc.d methuselah defaults
        sudo service methuselah start

output " "
output "Looks like everything installed correctly"
output " "
output "to start your master node you need to start Methuselah Using"
output " "
output "                service methuselah start                "
output " "
output " Once the daemon has started you can use"
output " "
output " service methuselah master <gives masternode status> "
output " service methuselah chain <gives blockchain info>    "
output " service methuselah status <gives the status of methuselahd"
output " "
output " here is a full list of usage's"
output ""
output " {start|stop|status|restart|chain|master|mnpeers} "
output ""
output " Now that the VPS Masternode is up and running you need to "
output " configure your masternode.conf file locally where you have"
output " your wallet installed. Under help select 'show masternode Conf File'"
output ""
output " each line of this conf file will be a MN setup that will"
output " be displayed or rather needs to be structured like: "
output ""
output " masternode_alias ip_address_and_port masternode_key transaction_id transaction_output "
output ""
output " Example : (( shortend masternode key and transaction id...just showing structure))"
output " mn1 45.77.158.182:7555 92YQFVwhDUv7ofyzAfW89k3ZFwG 6d8c2b91dfffddc74aac86f3b0e7b76c5 0 "
output ""
output " only a single space is needed between values "
output ""
output " save the file and restart your wallet,  Check 'My Masternodes' tab "
output " make sure it is listed. if it does not show click START MISSING"
output " if you recieve error about vin run this command in debug console"
output ""
output " lockunspent true"
output ""
output "reboot your VPS after reboot methuselah will be started on boot"
